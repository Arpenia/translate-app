# Translate App

Simple translate application using Google Translate API

## Description

This application is a simple web based application build on [AdonisJS](https://adonisjs.com/) using [Typescript](https://www.typescriptlang.org/). You can input any language support by [Google Support Language](https://cloud.google.com/translate/docs/languages) ( `sl = 'auto'`) and you can translate it to Indonesian, English, or Korean by choosing one of them.

## Test and Deploy

To deploy this app locally, type in git bash or command prompt.

```bash
npm i
node ace serve --watch
```

After that, open [`http://localhost:3333/`](http://localhost:3333/)
